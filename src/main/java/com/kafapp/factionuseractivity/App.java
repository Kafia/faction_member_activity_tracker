package com.kafapp.factionuseractivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

/**
 * Hello world!
 *
 */

@Slf4j
public class App {

	private static ObjectMapper objectMapper = new ObjectMapper();

	public static void main(String[] args) {
		System.out.println("Hello World!");

		// Read the excel file
		Workbook excel = null;

		FileInputStream inputStream = null;

		try {

			// excel = WorkbookFactory.create(new
			// File("src/main/resources/KafApp_UserActivityLog.xlsx"));

			inputStream = new FileInputStream(new File("src/main/resources/KafApp_UserActivityLog.xlsx"));
			excel = WorkbookFactory.create(inputStream);

		} catch (EncryptedDocumentException e) {
			log.error("File is encrypted", e);
		} catch (IOException e) {
			log.error("Could not read the excel file", e);
		}

		if (excel == null) {
			System.exit(0);
		}

		// Read the excel sheets
		Sheet refDataSheet = null;
		Sheet activityRefData = null;
		Sheet readableActivityLog = null;

		// Reference Data Cells
		Cell apiKeyCell = null;
		Cell factionIdCell = null;
		Cell factionMembersCell = null;

		// Reference Data
		String apiKey = null;
		int factionId = 0;
		String factionMembersStr = null;
		Set<String> factionMembersSet = new HashSet<>();
		HashMap<String, String> factionMembersIdName = new HashMap<>();

		HashMap<String, Integer> factionMembersNameActivity = new HashMap<>();

		try {

			// Get sheets
			refDataSheet = excel.getSheet("Basic_Config");
			activityRefData = excel.getSheet("Activity_Track");
			readableActivityLog = excel.getSheet("Readable_Activity_Log");

			// Get reference data
			apiKeyCell = refDataSheet.getRow(0).getCell(1);
			factionIdCell = refDataSheet.getRow(1).getCell(1);
			factionMembersCell = refDataSheet.getRow(2).getCell(1);

			// Process reference data
			apiKey = apiKeyCell.getStringCellValue();
			factionId = (int) factionIdCell.getNumericCellValue();

			if (factionMembersCell == null) { // If program is being run for the first time.
				factionMembersCell = refDataSheet.getRow(2).createCell(1);
			} else {
				factionMembersStr = factionMembersCell.getStringCellValue();
			}

			TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {
			};

			// Fetch all members from excel sheet
			factionMembersIdName = objectMapper.readValue(factionMembersStr, typeRef);

			factionMembersSet = new HashSet<>(Arrays.asList(factionMembersStr.split(",")));

		} catch (Exception e) {
			log.error("Reference Data Error", e);
		}

		// for API calls
		Client client = ClientBuilder.newClient(new ClientConfig());

		// For the faction API
		WebTarget tornApi = client.target("https://api.torn.com/");
		WebTarget factionApi = tornApi.path("faction/" + factionId).queryParam("selections", "basic").queryParam("key",
				apiKey);

		try {
			Invocation.Builder invocateFactionApi = factionApi.request();

			// Call the faction API
			Response response = invocateFactionApi.get();

			// Read faction response as a string
			String factionJsonData = response.readEntity(String.class);

			// convert to JSON
			System.out.println(factionJsonData);

			// Convert to a JSON object so that I'm being able to play around with it in
			// code
			JsonNode factionData = objectMapper.readTree(factionJsonData);
			handleErrorMessageIfAny(factionData); // For API Errors

			// Get all faction members
			Iterator<Entry<String, JsonNode>> iter = factionData.get("members").fields();

			while (iter.hasNext()) {

				Entry<String, JsonNode> member = iter.next();

				// Iterating over faction members and putting them in a hashmap
				factionMembersIdName.put(member.getKey(), member.getValue().get("name").asText());

			}

			// System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(factionMembersIdName));

			// writing it in the excel
			factionMembersCell.setCellValue(
					objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(factionMembersIdName));

			// Now I'm gonna call the API for each faction member - past and present
			for (String memberId : factionMembersIdName.keySet()) {

				WebTarget userApi = tornApi.path("user/" + memberId).queryParam("selections", "personalstats")
						.queryParam("key", apiKey);

				Invocation.Builder invocateUserApi = userApi.request();

				Thread.sleep(1000); // Wait for 1 second between each API call.
				Response userResponse = invocateUserApi.get(); // call the user API

				// Read faction response as a string
				String userJsonData = userResponse.readEntity(String.class);

				// convert to JSON
				System.out.println(userResponse);

				JsonNode userData = objectMapper.readTree(userJsonData);
				handleErrorMessageIfAny(userData);

				// System.out.println("Member Name" + factionMembersIdName.get(memberId) + "
				// |Timestamp"
				// + userData.get("personalstats").get("useractivity").asInt());

				// get their activity and map it against their name
				factionMembersNameActivity.put(factionMembersIdName.get(memberId),
						userData.get("personalstats").get("useractivity").asInt());

			}

			// Now excel stuff starts

			// If program is run for the first time, I gotta create the header row in
			// "Activity Log" sheet
			if (activityRefData.getFirstRowNum() == -1) {

				Row headerRow = activityRefData.createRow(0);

				// Check cell A1 in the sheet. This part writes "Timestamp" on it.
				headerRow.createCell(0).setCellValue("TimeStamp");

				int column = 1;
				for (String memberName : factionMembersNameActivity.keySet()) {
					// Now I'm writing all member names in the sheet
					headerRow.createCell(column++).setCellValue(memberName);

				}

			}

			else if (activityRefData.getFirstRowNum() != -1) {
				// If program has been run earlier

				Set<String> existingMembers = new HashSet<String>();

				for (int i = 1; i < activityRefData.getRow(0).getLastCellNum(); i++) {

					// I'm getting all the existing members from the excel
					existingMembers.add(activityRefData.getRow(0).getCell(i).getStringCellValue());

				}

				// These are ALL the members, past and present. The "existingMembers" are the
				// ones which are already present in the excel
				ArrayList<String> newMembers = new ArrayList<>(
						Arrays.asList(factionMembersNameActivity.keySet().toArray(new String[] {})));

				// Keeping just the newly joined members in "newMembers" variable
				existingMembers.forEach(member -> newMembers.remove(member));

				int j = 0;
				for (int i = activityRefData.getRow(0).getLastCellNum(); i <= newMembers.size() + existingMembers.size(); i++) {

					// Write the new members in the excel
					activityRefData.getRow(0).createCell(i).setCellValue(newMembers.get(j++));

				}

			}

			// Creating a new empty row in the excel to write their new activity timestamps
			Row newRow = activityRefData.createRow(activityRefData.getLastRowNum() + 1);

			// Timestamp
			newRow.createCell(0).setCellValue(Instant.now().toString());

			Row headerRow = activityRefData.getRow(0);

			for (int i = 1; i <= factionMembersNameActivity.keySet().size(); i++) {

				// Going through the columns, checking the member name in the first row, and
				// then writing their new activity timestamp in the new row
				String headerMemberName = headerRow.getCell(i).getStringCellValue();

				int memberActivity = factionMembersNameActivity.get(headerMemberName);

				newRow.createCell(i).setCellValue(memberActivity);

			}

			inputStream.close();

			
			//Write the excel
			OutputStream fileOut = new FileOutputStream("src/main/resources/KafApp_UserActivityLog.xlsx");
			excel.write(fileOut);
			excel.close();
			fileOut.close();

		} catch (Exception e) {
			log.error("Could not fetch faction members. Exiting");
			e.printStackTrace();
			System.exit(1);
		}

	}

	private static void handleErrorMessageIfAny(JsonNode apiResponse) throws InterruptedException {

		if (!apiResponse.has("error")) {
			return;
		} else {

			System.out.println("Torn API Returned Error\n" + apiResponse.toPrettyString());

			int errorCode = apiResponse.get("error").get("code").asInt();

			switch (errorCode) {

			case 0:
				System.exit(0);
			case 1:
				System.exit(0);
			case 2:
				System.exit(0);
			case 3:
				System.out.println("Contact Developer");
			case 4:
				System.out.println("Contact Developer");
			case 5:
				Thread.sleep(30000);
			case 6:
				System.exit(0);
			case 7:
				System.out.println("Your API key doesn't have permission to access this data");
			case 8:
				System.exit(0);
			case 9:
				System.exit(0);
			case 10:
				System.exit(0);
			case 11:
				System.exit(0);
			case 12:
				System.exit(0);

			}

		}

	}

}
